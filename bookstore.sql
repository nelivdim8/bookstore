-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: bookstore
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `book`
--

DROP TABLE IF EXISTS `book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `book` (
  `item_id` int(11) NOT NULL,
  `title` varchar(40) NOT NULL,
  `author_name` varchar(40) NOT NULL,
  `is_electornic` tinyint(1) NOT NULL,
  PRIMARY KEY (`item_id`),
  CONSTRAINT `fk_item_book` FOREIGN KEY (`item_id`) REFERENCES `catalog_items` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book`
--

LOCK TABLES `book` WRITE;
/*!40000 ALTER TABLE `book` DISABLE KEYS */;
INSERT INTO `book` VALUES (1,'Neka ti razkaja','Horke Bukai',0),(3,'Lqlq','Belote',0),(4,'Lqlq','Belote',0),(7,'Lqlq','Belote',0),(8,'Book','Science',0);
/*!40000 ALTER TABLE `book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_items`
--

DROP TABLE IF EXISTS `catalog_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `catalog_items` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_type` enum('Book','Game') DEFAULT NULL,
  `price` float(5,2) DEFAULT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_items`
--

LOCK TABLES `catalog_items` WRITE;
/*!40000 ALTER TABLE `catalog_items` DISABLE KEYS */;
INSERT INTO `catalog_items` VALUES (1,'Book',2.00),(2,'Game',10.00),(3,'Book',4.00),(4,'Book',4.00),(5,'Book',4.00),(6,'Book',4.00),(7,'Book',4.00),(8,'Book',4.00);
/*!40000 ALTER TABLE `catalog_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `client_request`
--

DROP TABLE IF EXISTS `client_request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `client_request` (
  `username` varchar(30) NOT NULL,
  `catalog_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `status` enum('Requested','Accepted','Sent') NOT NULL,
  KEY `username` (`username`),
  KEY `catalog_id` (`catalog_id`),
  CONSTRAINT `client_request_ibfk_1` FOREIGN KEY (`username`) REFERENCES `user` (`username`),
  CONSTRAINT `client_request_ibfk_2` FOREIGN KEY (`catalog_id`) REFERENCES `catalog_items` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_request`
--

LOCK TABLES `client_request` WRITE;
/*!40000 ALTER TABLE `client_request` DISABLE KEYS */;
/*!40000 ALTER TABLE `client_request` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game`
--

DROP TABLE IF EXISTS `game`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `game` (
  `item_id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `min_number_of_players` int(10) unsigned NOT NULL,
  `max_number_of_players` int(10) unsigned NOT NULL,
  PRIMARY KEY (`item_id`),
  CONSTRAINT `fk_game_id` FOREIGN KEY (`item_id`) REFERENCES `catalog_items` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game`
--

LOCK TABLES `game` WRITE;
/*!40000 ALTER TABLE `game` DISABLE KEYS */;
/*!40000 ALTER TABLE `game` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventory`
--

DROP TABLE IF EXISTS `inventory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `inventory` (
  `item_id` int(11) NOT NULL,
  `quantity` int(10) unsigned NOT NULL,
  PRIMARY KEY (`item_id`),
  CONSTRAINT `fk_item` FOREIGN KEY (`item_id`) REFERENCES `catalog_items` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventory`
--

LOCK TABLES `inventory` WRITE;
/*!40000 ALTER TABLE `inventory` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `request_for_new_book`
--

DROP TABLE IF EXISTS `request_for_new_book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `request_for_new_book` (
  `id` int(11) NOT NULL,
  `author` varchar(30) NOT NULL,
  `title` varchar(30) NOT NULL,
  `is_electronic` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_id` FOREIGN KEY (`id`) REFERENCES `request_for_new_item` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `request_for_new_book`
--

LOCK TABLES `request_for_new_book` WRITE;
/*!40000 ALTER TABLE `request_for_new_book` DISABLE KEYS */;
/*!40000 ALTER TABLE `request_for_new_book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `request_for_new_game`
--

DROP TABLE IF EXISTS `request_for_new_game`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `request_for_new_game` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `min_number_of_players` int(11) NOT NULL,
  `max_number_of_players` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_id_game` FOREIGN KEY (`id`) REFERENCES `request_for_new_item` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `request_for_new_game`
--

LOCK TABLES `request_for_new_game` WRITE;
/*!40000 ALTER TABLE `request_for_new_game` DISABLE KEYS */;
/*!40000 ALTER TABLE `request_for_new_game` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `request_for_new_item`
--

DROP TABLE IF EXISTS `request_for_new_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `request_for_new_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `type` enum('Book','Game') NOT NULL,
  `quantity` int(11) NOT NULL,
  `status` enum('Requested','Accepted','Sent') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_username` (`username`),
  CONSTRAINT `fk_username` FOREIGN KEY (`username`) REFERENCES `user` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `request_for_new_item`
--

LOCK TABLES `request_for_new_item` WRITE;
/*!40000 ALTER TABLE `request_for_new_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `request_for_new_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `first_name` varchar(40) NOT NULL,
  `last_name` varchar(40) NOT NULL,
  `email` varchar(40) NOT NULL,
  `role` enum('Customer','Staff') NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `warehouse_order`
--

DROP TABLE IF EXISTS `warehouse_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `warehouse_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_catalog` int(11) DEFAULT NULL,
  `id_new` int(11) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_id_catalog` (`id_catalog`),
  KEY `fk_id_new` (`id_new`),
  CONSTRAINT `fk_id_catalog` FOREIGN KEY (`id_catalog`) REFERENCES `catalog_items` (`item_id`),
  CONSTRAINT `fk_id_new` FOREIGN KEY (`id_new`) REFERENCES `request_for_new_item` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `warehouse_order`
--

LOCK TABLES `warehouse_order` WRITE;
/*!40000 ALTER TABLE `warehouse_order` DISABLE KEYS */;
/*!40000 ALTER TABLE `warehouse_order` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-21 11:29:13
