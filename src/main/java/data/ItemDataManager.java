package data;

import java.sql.SQLException;
import java.util.List;

import items.Item;
import items.Order;

public interface ItemDataManager {
    boolean purchaseItemById(int id, int quantity, String username) throws SQLException;

    List<Order> getRequestedItems(String username) throws SQLException;

    List<Item> showItems() throws SQLException;

    boolean orderAnItem(int id, int quantity) throws SQLException;

    boolean requestForAnItem(int id, int quantity, String username) throws SQLException;

}
