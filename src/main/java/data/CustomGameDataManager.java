package data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import items.BoardGame;
import items.Item;
import items.ItemType;
import items.Order;
import items.Status;
import users.Member;

public class CustomGameDataManager extends AbstractDataItem implements GameDataManager {

    public CustomGameDataManager(DataConnection connection) {
        super(connection);
        // TODO Auto-generated constructor stub
    }

    @Override
    public List<Item> getGameByName(String name) throws SQLException {

        String query = "select catalog_items.item_id, catalog_items.item_type,catalog_items.price,game.name,game.min_number_of_players,"
                + "game.max_number_of_players from catalog_items inner join game "
                + "on catalog_items.item_id=game.item_id where game.name regexp" + "'.*" + name + ".*'";
        Connection connection = super.getConnection().getConnection();
        PreparedStatement pstmt = connection.prepareStatement(query);
        ResultSet result = pstmt.executeQuery();
        List<Item> items = storeItems(result);
        super.getConnection().closeConnection(connection, pstmt, result);
        return items;
    }

    @Override
    public List<Item> getGameByNumberOfPlayers(int minNumber, int maxNumber) throws SQLException {
        String query = "select catalog_items.item_id, catalog_items.item_type,catalog_items.price,game.name,game.min_number_of_players,"
                + "game.max_number_of_players from catalog_items inner join game "
                + "on catalog_items.item_id=game.item_id where game.min_number_of_players=? AND game.max_number_of_players = ?;";
        Connection connection = super.getConnection().getConnection();
        PreparedStatement pstmt = connection.prepareStatement(query);
        pstmt.setInt(1, minNumber);
        pstmt.setInt(2, maxNumber);
        ResultSet result = pstmt.executeQuery();

        List<Item> items = storeItems(result);
        super.getConnection().closeConnection(connection, pstmt, result);
        return items;
    }

    private List<Item> storeItems(ResultSet result) throws SQLException {
        List<Item> items = new ArrayList<>();
        while (result.next()) {

            items.add(new BoardGame(result.getInt(1), result.getDouble(3), result.getString(4), result.getInt(5),
                    result.getInt(6)));
        }
        return items;
    }

    @Override
    public List<Order> getRequestedItems(String username) throws SQLException {
        List<Order> gameOrders = getNewRequestedGames(username);
        gameOrders.addAll(getRequestedGamesOutOfStock(username));
        return gameOrders;
    }

    private List<Order> getNewRequestedGames(String username) throws SQLException {
        List<Order> orders = new ArrayList<>();
        String query = "select request_for_new_item.id,request_for_new_game.name, request_for_new_game.min_number_of_players,"
                + "request_for_new_game.max_number_of_players,request_for_new_item.quantity,request_for_new_item.status"
                + " from request_for_new_item "
                + "inner join request_for_new_game on request_for_new_game.id=request_for_new_item.id where username='georgi_123';";
        Connection connection = super.getConnection().getConnection();
        PreparedStatement pstmt = connection.prepareStatement(query);
        pstmt.setString(1, username);
        ResultSet result = pstmt.executeQuery(query);
        while (result.next()) {
            Item item = new BoardGame(result.getInt(1), result.getDouble(2), result.getString(3), result.getInt(4),
                    result.getInt(5));
            Order order = new Order(item, result.getInt(6), username, Status.valueOf(result.getString(7)));
            orders.add(order);

        }

        super.getConnection().closeConnection(connection, pstmt, result);
        return orders;
    }

    private List<Order> getRequestedGamesOutOfStock(String username) throws SQLException {
        List<Order> orders = new ArrayList<>();
        String query = "select game.item_id, catalog_items.price,game.name, game.min_number_of_players,game.max_number_of_players,"
                + "client_request.quantity,client_request.status from client_request inner join game on game.item_id=client_request.catalog_id"
                + " inner join catalog_items"
                + " on game.item_id=catalog_items.item_id where client_request.username=?";
        Connection connection = super.getConnection().getConnection();
        PreparedStatement pstmt = connection.prepareStatement(query);
        pstmt.setString(1, username);
        ResultSet result = pstmt.executeQuery(query);
        while (result.next()) {
            Item item = new BoardGame(result.getInt(1), result.getDouble(2), result.getString(3), result.getInt(4),
                    result.getInt(5));
            Order order = new Order(item, result.getInt(6), username, Status.valueOf(result.getString(7)));
            orders.add(order);

        }
        super.getConnection().closeConnection(connection, pstmt, result);
        return orders;

    }

    @Override
    public boolean requestForAGame(Member member, String name, int minNumberOfPlayers, int maxNumberOfPlayers,
            int quantity) throws SQLException {
        String query = "select item_id from game where name=? and min_number_of_players=? and max_number_of_players=?";
        Connection connection = super.getConnection().getConnection();
        PreparedStatement pstmt = connection.prepareStatement(query);
        pstmt.setString(1, name);
        pstmt.setInt(2, minNumberOfPlayers);
        pstmt.setInt(3, maxNumberOfPlayers);
        int id = 0;
        ResultSet result = pstmt.executeQuery();
        if (result.next()) {
            id = result.getInt(1);
            insertAnOrder(id, quantity, member.getUsername(), Status.Requested);
            return true;

        }
        super.getConnection().closeConnection(connection, pstmt, result);
        return requestForANewGame(member, name, minNumberOfPlayers, maxNumberOfPlayers, quantity);
    }

    private boolean requestForANewGame(Member member, String name, int minNumberOfPlayers, int maxNumberOfPlayers,
            int quantity) throws SQLException {
        String query = "insert into request_for_new_item (username,type,quantity,status) values(?,?,?,?)";
        Connection connection = super.getConnection().getConnection();
        PreparedStatement pstmt = connection.prepareStatement(query);
        pstmt.setString(1, member.getUsername());
        pstmt.setString(2, ItemType.Game.getName());
        pstmt.setInt(3, quantity);
        pstmt.setString(4, Status.Requested.getName());
        pstmt.executeUpdate();
        query = "insert into request_for_new_game values((select max(id) from request_for_new_item),?,?,?)";
        pstmt = connection.prepareStatement(query);
        pstmt.setString(1, name);
        pstmt.setInt(2, minNumberOfPlayers);
        pstmt.setInt(3, maxNumberOfPlayers);
        int res = pstmt.executeUpdate();
        super.getConnection().closeConnection(connection, pstmt, null);
        return res > 0;
    }

    public boolean insertAGame(BoardGame game) throws SQLException, EntryAlreadyExistsException {
        if (itemExists(game.getName(), game.getMinNumberOfPlayers(), game.getMaxNumberOfPlayers())) {
            throw new EntryAlreadyExistsException("The game already exists!");
        }
        String query = "insert into `catalog_items` (item_type,price) values(?,?);";
        Connection connection = super.getConnection().getConnection();
        PreparedStatement pstmt = connection.prepareStatement(query);
        pstmt.setString(2, ItemType.Book.getName());
        pstmt.setDouble(3, game.getPrice());
        pstmt.executeUpdate();

        query = "insert into `game` values(?,?,?,?);";
        pstmt = super.getConnection().getConnection().prepareStatement(query);
        pstmt.setInt(1, game.getId());
        pstmt.setString(2, game.getName());
        pstmt.setInt(3, game.getMinNumberOfPlayers());
        pstmt.setInt(4, game.getMaxNumberOfPlayers());
        pstmt.executeUpdate();
        super.getConnection().closeConnection(connection, pstmt, null);

        return true;
    }

    private boolean itemExists(String name, int minNumberOfPlayers, int maxNumberOfPlayers) throws SQLException {
        String query = "select item_id from game where name=? and min_number_of_players=? and max_number_of_players=?";
        Connection connection = super.getConnection().getConnection();
        PreparedStatement pstmt = connection.prepareStatement(query);
        pstmt.setString(1, name);
        pstmt.setInt(2, minNumberOfPlayers);
        pstmt.setInt(3, maxNumberOfPlayers);
        ResultSet result = pstmt.executeQuery();
        boolean itemExists = result.next();
        super.getConnection().closeConnection(connection, pstmt, result);
        return itemExists;
    }

    @Override
    public List<Item> showItems() throws SQLException {
        List<Item> items = new ArrayList<>();
        String query = "select catalog_items.item_id, catalog_items.item_type,catalog_items.price,game.name,game.min_number_of_players,"
                + "game.max_number_of_players from catalog_items inner join game "
                + "on catalog_items.item_id=game.item_id";
        Connection connection = super.getConnection().getConnection();
        PreparedStatement pstmt = connection.prepareStatement(query);

        ResultSet result = pstmt.executeQuery(query);
        while (result.next()) {
            Item item = new BoardGame(result.getInt(1), result.getDouble(2), result.getString(3), result.getInt(4),
                    result.getInt(5));

        }
        super.getConnection().closeConnection(connection, pstmt, result);
        return items;
    }

}
