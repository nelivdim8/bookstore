package data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import users.Member;
import users.MembersFactory;
import users.Role;
import users.UnexistingRoleException;

public class CustomMemberDataManager implements MemberDataManager {
    private DataConnection connection;

    public CustomMemberDataManager(DataConnection connection) {
        super();
        this.connection = connection;
    }

    @Override
    public boolean registerUser(String firstName, String lastName, String username, String password, String email,
            Role role) throws SQLException {
        String query = "insert into user(username,password,first_name,last_name,email,role) values(?,?,?,?,?,?)";
        Connection conn = connection.getConnection();
        PreparedStatement pstmt = conn.prepareStatement(query);
        pstmt.setString(1, username);
        pstmt.setString(2, password);
        pstmt.setString(3, firstName);
        pstmt.setString(4, lastName);
        pstmt.setString(5, email);
        pstmt.setString(6, role.getName());
        boolean res = pstmt.executeUpdate() > 0;
        connection.closeConnection(conn, pstmt, null);
        return res;
    }

    @Override
    public Member logIn(String username, String password)
            throws SQLException, UnexistingUserException, UnexistingRoleException {
        String query = "select * from user where username=? and password=?";
        Connection conn = connection.getConnection();
        PreparedStatement pstmt = conn.prepareStatement(query);
        pstmt.setString(1, username);
        pstmt.setString(2, password);
        ResultSet result = pstmt.executeQuery();
        if (result.next()) {
            Role role = Role.valueOf(result.getString("role"));
            return MembersFactory.getInstance().getUserByRole(role).createMember(result.getString("first_name"),
                    result.getString("last_name"), username, password, result.getString("email"), role);

        }
        connection.closeConnection(conn, pstmt, result);
        throw new UnexistingUserException();

    }

}
