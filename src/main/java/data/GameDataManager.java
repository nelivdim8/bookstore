package data;

import java.sql.SQLException;
import java.util.List;

import items.BoardGame;
import items.Item;
import users.Member;

public interface GameDataManager extends ItemDataManager {
    List<Item> getGameByName(String name) throws SQLException;

    List<Item> getGameByNumberOfPlayers(int minNumber, int maxNumber) throws SQLException;

    boolean requestForAGame(Member member, String name, int minNumberOfPlayers, int maxNumberOfPlayers, int quantity)
            throws SQLException;

    boolean insertAGame(BoardGame game) throws SQLException, EntryAlreadyExistsException;

}
