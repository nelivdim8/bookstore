package data;

import java.sql.SQLException;
import java.util.List;

import items.Book;
import items.Item;
import users.Member;

public interface BookDataManager extends ItemDataManager {
    List<Item> getBookByAuthorName(String authorName) throws SQLException;

    List<Item> getBookByTitle(String title) throws SQLException;

    boolean requestForABook(Member member, String title, String author, int quantity, boolean isElectronic)
            throws SQLException;

    boolean insertABook(Book book) throws SQLException;

}
