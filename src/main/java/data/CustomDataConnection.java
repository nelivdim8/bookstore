package data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class CustomDataConnection extends DataConnection {

    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:mysql://localhost:3306/bookstore?autoReconnect=true&useSSL=false",
                "root", "123456");

    }

}
