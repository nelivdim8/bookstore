package data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import items.Status;

public abstract class AbstractDataItem implements ItemDataManager {
    private DataConnection connection;

    public AbstractDataItem(DataConnection connection) {
        super();
        this.connection = connection;
    }

    public DataConnection getConnection() {
        return connection;
    }

    public void insertAnOrder(int itemId, int quantity, String username, Status status) throws SQLException {
        String query = "insert into client_request values(?,?,?,?);";
        Connection conn = connection.getConnection();
        PreparedStatement pstmt = conn.prepareStatement(query);
        pstmt.setInt(2, itemId);
        pstmt.setInt(3, quantity);
        pstmt.setString(1, username);
        pstmt.setString(4, status.getName());

        pstmt.executeUpdate();
        connection.closeConnection(conn, pstmt, null);

    }

    @Override
    public boolean purchaseItemById(int id, int quantity, String username) throws SQLException {
        int quantityAvailable = getQuantity(id, quantity);
        if (quantityAvailable < quantity) {
            return false;
        }
        insertAnOrder(id, quantity, username, Status.Accepted);
        updateInventory(id, quantityAvailable - quantity);
        return true;

    }

    public boolean requestForAnItem(int id, int quantity, String username) throws SQLException {
        insertAnOrder(id, quantity, username, Status.Requested);
        return true;

    }

    private int getQuantity(int id, int quantity) throws SQLException {
        String query = "select quantity from inventory where item_id=?";
        Connection conn = connection.getConnection();
        PreparedStatement pstmt = conn.prepareStatement(query);
        pstmt.setInt(1, id);
        ResultSet result = pstmt.executeQuery();

        int quantityAvailable = 0;
        while (result.next()) {
            quantityAvailable = result.getInt(1);
        }
        connection.closeConnection(conn, pstmt, result);
        return quantityAvailable;

    }

    private void updateInventory(int id, int quantityAvailable) throws SQLException {
        String query = "update inventory set quantity=?  where item_id=?";
        Connection conn = connection.getConnection();
        PreparedStatement pstmt = conn.prepareStatement(query);
        pstmt.setInt(1, quantityAvailable);
        pstmt.setInt(2, id);
        pstmt.executeUpdate();
        connection.closeConnection(conn, pstmt, null);

    }

    public boolean orderAnItem(int id, int quantity) throws SQLException {
        String query = "select item_id from catalog_items where item_id=?";
        Connection conn = connection.getConnection();
        PreparedStatement pstmt = conn.prepareStatement(query);
        pstmt.setInt(1, id);
        ResultSet result = pstmt.executeQuery();
        if (!result.next()) {
            connection.closeConnection(conn, pstmt, null);
            return orderFromWarehouseHelper(id, quantity, "insert into warehouse_order (id_new,quantity) values(?,?)");

        }
        connection.closeConnection(conn, pstmt, null);
        return orderFromWarehouseHelper(id, quantity, "insert into warehouse_order (id_catalog,quantity) values(?,?)");

    }

    private boolean orderFromWarehouseHelper(int id, int quantity, String query) throws SQLException {
        Connection conn = connection.getConnection();
        PreparedStatement pstmt = conn.prepareStatement(query);
        pstmt.setInt(1, id);
        pstmt.setInt(2, quantity);
        boolean flag = pstmt.executeUpdate() > 0;
        connection.closeConnection(conn, pstmt, null);
        return flag;

    }

}
