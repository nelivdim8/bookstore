package data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class DataConnection {
    public void closeConnection(Connection connection, PreparedStatement statement, ResultSet result) {
        try {
            if (result != null)
                result.close();
            if (statement != null)
                statement.close();
            if (connection != null)
                connection.close();
        } catch (SQLException e) {

        }
    }

    public abstract Connection getConnection() throws SQLException;
}
