package data;

import java.sql.SQLException;

import users.Member;
import users.Role;
import users.UnexistingRoleException;

public interface MemberDataManager {
    boolean registerUser(String firstName, String lastName, String username, String password, String email, Role role)
            throws SQLException;

    Member logIn(String username, String password)
            throws SQLException, UnexistingUserException, UnexistingRoleException;

}
