package data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import items.BoardGame;
import items.Book;
import items.Item;
import items.ItemType;
import items.Order;
import items.Status;
import users.Member;

public class CustomBookDataManager extends AbstractDataItem implements BookDataManager {

    public CustomBookDataManager(DataConnection connection) {
        super(connection);
    }

    @Override
    public List<Item> getBookByAuthorName(String authorName) throws SQLException {
        String query = "select catalog_items.item_id, catalog_items.item_type,catalog_items.price,book.title,"
                + "book.author_name,"
                + "book.is_electronic from catalog_items inner join book on catalog_items.item_id=book.item_id where book.author_name=?";
        Connection connection = super.getConnection().getConnection();
        PreparedStatement pstmt = connection.prepareStatement(query);
        pstmt.setString(1, authorName);
        ResultSet result = pstmt.executeQuery();
        List<Item> items = new ArrayList<>();
        while (result.next()) {
            items.add(new Book(result.getInt(1), result.getDouble(3), result.getString(4), result.getString(5),
                    result.getInt(6)));

        }
        super.getConnection().closeConnection(connection, pstmt, result);

        return items;
    }

    @Override
    public List<Item> getBookByTitle(String title) throws SQLException {
        String query = "select catalog_items.item_id, catalog_items.item_type,catalog_items.price,book.title,"
                + "book.author_name," + "book.is_electronic from catalog_items inner join book "
                + "on catalog_items.item_id=book.item_id where book.title regexp" + "'.*" + title + ".*'";
        Connection connection = super.getConnection().getConnection();
        PreparedStatement pstmt = connection.prepareStatement(query);
        ResultSet result = pstmt.executeQuery();
        List<Item> items = storeItems(result);
        super.getConnection().closeConnection(connection, pstmt, result);
        return items;
    }

    private List<Item> storeItems(ResultSet result) throws SQLException {
        List<Item> items = new ArrayList<>();
        while (result.next()) {
            items.add(new Book(result.getInt(1), result.getDouble(3), result.getString(4), result.getString(5),
                    result.getInt(6)));

        }
        return items;

    }

    @Override
    public boolean insertABook(Book book) throws SQLException {
        String query = "insert into `catalog_items` (item_type,price) values(?,?);";
        Connection connection = super.getConnection().getConnection();
        PreparedStatement pstmt = connection.prepareStatement(query);
        pstmt.setString(2, ItemType.Book.getName());
        pstmt.setDouble(3, book.getPrice());
        int value = pstmt.executeUpdate();
        if (value < 0) {
            return false;
        }
        query = "insert into `game` values(?,?,?,?);";
        pstmt = super.getConnection().getConnection().prepareStatement(query);
        pstmt.setInt(1, book.getId());
        pstmt.setString(2, book.getTitle());
        pstmt.setString(3, book.getAuthor());
        pstmt.setBoolean(4, book.isElectronic());
        pstmt.executeUpdate();

        return true;
    }

    @Override
    public boolean requestForABook(Member member, String title, String author, int quantity, boolean isElectronic)
            throws SQLException {
        String query = "select item_id from book where title=? and author_name=?";
        Connection connection = super.getConnection().getConnection();
        PreparedStatement pstmt = connection.prepareStatement(query);
        pstmt.setString(1, title);
        pstmt.setString(2, author);
        int id = 0;
        ResultSet result = pstmt.executeQuery();
        if (result.next()) {
            id = result.getInt(1);
            insertAnOrder(id, quantity, member.getUsername(), Status.Requested);
            return true;

        }
        return requestForANewBook(member, title, author, quantity, isElectronic);

    }

    private boolean requestForANewBook(Member member, String title, String author, int quantity, boolean isElectronic)
            throws SQLException {
        String query = "insert into request_for_new_item (username,type,quantity,status) values(?,?,?,?)";
        Connection connection = super.getConnection().getConnection();
        PreparedStatement pstmt = connection.prepareStatement(query);
        pstmt.setString(1, member.getUsername());
        pstmt.setString(2, ItemType.Book.getName());
        pstmt.setInt(3, quantity);
        pstmt.setString(4, Status.Requested.getName());
        pstmt.executeUpdate();
        query = "insert into request_for_new_book values((select max(id) from request_for_new_item),?,?,?)";
        pstmt = super.getConnection().getConnection().prepareStatement(query);
        pstmt.setString(1, author);
        pstmt.setString(2, title);
        pstmt.setBoolean(3, isElectronic);
        return pstmt.executeUpdate() > 0;

    }

    private List<Order> getRequestedBooksOutOfStock(String username) throws SQLException {
        List<Order> orders = new ArrayList<>();
        String query = " select book.item_id, catalog_items.price,book.author_name, book.title,book.is_electronic,"
                + "client_request.quantity,client_request.status from client_request inner join book on book.item_id=client_request.catalog_id"
                + " inner join catalog_items"
                + " on book.item_id=catalog_items.item_id where client_request.username=?";
        Connection connection = super.getConnection().getConnection();
        PreparedStatement pstmt = connection.prepareStatement(query);
        pstmt.setString(1, username);
        ResultSet result = pstmt.executeQuery(query);
        while (result.next()) {
            Item item = new Book(result.getInt(1), result.getDouble(2), result.getString(3), result.getString(4),
                    result.getBoolean(5));
            Order order = new Order(item, result.getInt(6), username, Status.valueOf(result.getString(7)));
            orders.add(order);

        }
        return orders;

    }

    @Override
    public List<Order> getRequestedItems(String username) throws SQLException {
        List<Order> bookOrders = getNewRequestedBooks(username);
        bookOrders.addAll(getRequestedBooksOutOfStock(username));
        return bookOrders;
    }

    private List<Order> getNewRequestedBooks(String username) throws SQLException {
        List<Order> orders = new ArrayList<>();
        String query = "select request_for_new_item.id,request_for_new_book.author, request_for_new_book.title,"
                + "request_for_new_book.is_electronic,request_for_new_item.quantity,"
                + "request_for_new_item.status from request_for_new_item inner join"
                + " request_for_new_book on request_for_new_book.id=request_for_new_item.id where username=?";
        Connection connection = super.getConnection().getConnection();
        PreparedStatement pstmt = connection.prepareStatement(query);
        pstmt.setString(1, username);
        ResultSet result = pstmt.executeQuery(query);
        while (result.next()) {
            Item item = new Book(result.getInt(1), result.getDouble(2), result.getString(3), result.getString(4),
                    result.getBoolean(5));
            Order order = new Order(item, result.getInt(6), username, Status.valueOf(result.getString(7)));
            orders.add(order);

        }
        return orders;

    }

    @Override
    public List<Item> showItems() throws SQLException {
        List<Item> items = new ArrayList<>();
        String query = "select catalog_items.item_id, catalog_items.item_type,catalog_items.price,book.title,"
                + "book.author_name," + "book.is_electronic from catalog_items inner join book "
                + "on catalog_items.item_id=book.item_id";
        Connection connection = super.getConnection().getConnection();
        PreparedStatement pstmt = connection.prepareStatement(query);

        ResultSet result = pstmt.executeQuery(query);
        while (result.next()) {
            Item item = new BoardGame(result.getInt(1), result.getDouble(2), result.getString(3), result.getInt(4),
                    result.getInt(5));

        }
        return items;
    }

}
