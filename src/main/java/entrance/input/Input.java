package entrance.input;

public interface Input {
    String getInput();

    void closeResource();

}
