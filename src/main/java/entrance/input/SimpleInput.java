package entrance.input;

import java.util.Scanner;

public class SimpleInput implements Input {
    private static Scanner input = new Scanner(System.in);

    public String getInput() {
        return input.nextLine();

    }

    @Override
    public void closeResource() {
        input.close();

    }

}
