package entrance;

import java.sql.SQLException;

import entrance.input.Input;
import options.Option;
import options.OptionFactory;
import options.Result;
import users.User;

public class WebPage {
    private User user;
    private Input input;

    public WebPage(User user, Input input) {
        super();
        this.user = user;
        this.input = input;
    }

    public void open() {

        System.out.println("Welcome to bookstore!");
        while (!hasToExit()) {
            System.out.println("Enter an option:");
            String userInput = input.getInput();
            try {
                Option option = OptionFactory.getInstance().getOption(userInput);
                Result result = user.executeOption(option);

                if (result.getUser() != null) {
                    user = result.getUser();

                }
                System.out.println("Result: " + result.toString());
            } catch (SQLException e) {
                System.out.println("Option not accepted, try again!");
            }

        }
        input.closeResource();

    }

    private boolean hasToExit() {
        System.out.println("Do you want to continue? Enter yes or no!");

        return input.getInput().matches("no");

    }

}
