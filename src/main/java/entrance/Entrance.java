package entrance;

import entrance.input.SimpleInput;
import options.OptionFactory;
import options.account.LogInOption;
import options.account.RegisterOption;
import options.account.UnregisterOption;
import options.boardgame.RequestNewBoardGameOption;
import options.boardgame.SearchForGameByNameOption;
import options.boardgame.SearchForGameByNumberOfPlayersOption;
import options.book.RequestNewBookOption;
import options.book.SearchForBookByAuthorNameOption;
import options.book.SearchForBookByTitleOption;
import options.common.OrderInventoryOption;
import options.common.PurchaseAnItemOption;
import options.common.RequestOutOfStockOption;
import options.common.ShowRequestedItemsOption;
import users.Customer;
import users.Guest;
import users.MembersFactory;
import users.Role;
import users.Staff;
import users.UnexistingRoleException;

public class Entrance {
    public static void main(String[] args) {

        addOptions();
        addMembers();
        WebPage page = new WebPage(new Guest(), new SimpleInput());
        page.open();
    }

    private static void addMembers() {
        try {
            MembersFactory.getInstance().addEntry(Role.Customer, new Customer());
            MembersFactory.getInstance().addEntry(Role.Staff, new Staff());
        } catch (UnexistingRoleException e) {
            System.out.println("Unexisting role!");
        }

    }

    private static void addOptions() {
        OptionFactory.getInstance().addBoardGameOption(RequestNewBoardGameOption.getInputRegex(),
                new RequestNewBoardGameOption());
        OptionFactory.getInstance().addBoardGameOption(SearchForGameByNameOption.getInputRegex(),
                new SearchForGameByNameOption());
        OptionFactory.getInstance().addBoardGameOption(SearchForGameByNumberOfPlayersOption.getInputRegex(),
                new SearchForGameByNumberOfPlayersOption());
        OptionFactory.getInstance().addBookOption(RequestNewBookOption.getInputRegex(), new RequestNewBookOption());
        OptionFactory.getInstance().addBookOption(SearchForBookByAuthorNameOption.getInputRegex(),
                new SearchForBookByAuthorNameOption());
        OptionFactory.getInstance().addBookOption(SearchForBookByTitleOption.getInputRegex(),
                new SearchForBookByTitleOption());
        OptionFactory.getInstance().addCommonOption(OrderInventoryOption.getInputRegex(), new OrderInventoryOption());
        OptionFactory.getInstance().addCommonOption(PurchaseAnItemOption.getInputRegex(), new PurchaseAnItemOption());
        OptionFactory.getInstance().addCommonOption(RequestOutOfStockOption.getInputRegex(),
                new RequestOutOfStockOption());
        OptionFactory.getInstance().addCommonOption(ShowRequestedItemsOption.getInputRegex(),
                new ShowRequestedItemsOption());
        OptionFactory.getInstance().addAccountOption(LogInOption.getInputRegex(), new LogInOption());
        OptionFactory.getInstance().addAccountOption(UnregisterOption.getInputRegex(), new UnregisterOption());
        OptionFactory.getInstance().addAccountOption(RegisterOption.getInputRegex(), new RegisterOption());

    }
}
