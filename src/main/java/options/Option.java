package options;

import users.Customer;
import users.Guest;
import users.Staff;

public interface Option {
    Result execute(Guest guest);

    Result execute(Customer customer);

    Result execute(Staff staff);
}
