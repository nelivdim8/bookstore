package options.boardgame;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import data.GameDataManager;
import items.Item;
import options.Option;
import options.Result;
import users.Customer;
import users.Guest;
import users.Staff;

public class SearchForGameByNameOption extends BoardGameAbstractOption implements Option {
    private final static String INPUT_REGEX = "search board game by name '[A-Z][a-z\\s]*'";

    private SearchForGameByNameOption(GameDataManager gameData, String input) {
        super(gameData, input);

    }

    public SearchForGameByNameOption() {
        super();
    }

    public static String getInputRegex() {
        return INPUT_REGEX;
    }

    @Override
    public Result execute(Guest guest) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Result execute(Customer customer) {
        String[] values = super.splitInput(super.getInput());
        List<Item> games = new ArrayList<>();
        try {
            games = super.getGameData().getGameByName(values[0]);
        } catch (NumberFormatException | SQLException e) {
            return new Result<List<Item>>(games, false, "Unsuccessful search!", customer, false, e);
        }
        return new Result<List<Item>>(games, true, "Result from search: ", customer, false, null);
    }

    @Override
    public Result execute(Staff staff) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public BoardGameAbstractOption getOption(GameDataManager gameData, String input) {
        return new SearchForGameByNameOption(gameData, input);
    }
}