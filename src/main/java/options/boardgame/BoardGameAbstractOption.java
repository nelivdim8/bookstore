package options.boardgame;

import data.GameDataManager;
import options.AbstractOption;
import options.Option;

public abstract class BoardGameAbstractOption extends AbstractOption implements Option {
    private GameDataManager gameData;

    public BoardGameAbstractOption(GameDataManager gameData, String input) {
        super(input);
        this.gameData = gameData;

    }

    public BoardGameAbstractOption() {
        super();

    }

    public GameDataManager getGameData() {
        return gameData;
    }

    public abstract BoardGameAbstractOption getOption(GameDataManager gameData, String input);

}
