package options.boardgame;

import java.sql.SQLException;

import data.GameDataManager;
import options.Option;
import options.Result;
import users.Customer;
import users.Guest;
import users.Staff;

public class RequestNewBoardGameOption extends BoardGameAbstractOption implements Option {
    private final static String INPUT_REGEX = "request board game '[A-Z][a-z\\s].*' '[1-9]+[0-9]*' '[1-9]+[0-9]*' '[1-9]+[0-9]*'";

    private RequestNewBoardGameOption(GameDataManager gameData, String input) {
        super(gameData, input);

    }

    public RequestNewBoardGameOption() {
        super();
    }

    public static String getInputRegex() {
        return INPUT_REGEX;
    }

    @Override
    public Result execute(Guest guest) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Result execute(Customer customer) {
        String[] values = super.splitInput(super.getInput());
        boolean isSuccessful = false;
        try {
            isSuccessful = super.getGameData().requestForAGame(customer, values[0], Integer.parseInt(values[1]),
                    Integer.parseInt(values[2]), Integer.parseInt(values[3]));
        } catch (NumberFormatException | SQLException e) {
            return new Result<Boolean>(isSuccessful, false, "Unsuccessful request!", customer, false, e);
        }
        return new Result<Boolean>(isSuccessful, true, "Successful request! ", customer, false, null);
    }

    @Override
    public Result execute(Staff staff) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public BoardGameAbstractOption getOption(GameDataManager gameData, String input) {
        return new RequestNewBoardGameOption(gameData, input);
    }

}
