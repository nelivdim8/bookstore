package options;

public abstract class AbstractOption {
    private String input;

    public AbstractOption(String input) {
        super();
        this.input = input;
    }

    public AbstractOption() {
        super();

    }

    public String getInput() {
        return input;
    }

    public static String[] splitInput(String input) {
        int startIndex = input.indexOf('\'');
        System.out.println(startIndex);
        String inputSubstring = input.substring(startIndex + 1, input.length() - 1);
        String[] inputValues = inputSubstring.split("'\\s'");
        return inputValues;

    }
}
