package options;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import data.CustomBookDataManager;
import data.CustomDataConnection;
import data.CustomGameDataManager;
import data.CustomMemberDataManager;
import options.account.AbstractAccountOption;
import options.boardgame.BoardGameAbstractOption;
import options.book.BookAbstractOption;
import options.common.InvalidOption;
import options.common.ItemAbstractOption;

public class OptionFactory {
    private static OptionFactory factory = new OptionFactory();
    private Map<String, AbstractAccountOption> accountOptions;
    private Map<String, ItemAbstractOption> commonOptions;
    private Map<String, BookAbstractOption> bookOptions;
    private Map<String, BoardGameAbstractOption> boardGameOptions;

    private OptionFactory() {
        this.accountOptions = new HashMap<>();
        this.commonOptions = new HashMap<>();
        this.bookOptions = new HashMap<>();
        this.boardGameOptions = new HashMap<>();

    }

    public Option getOption(String input) throws SQLException {
        for (String s : accountOptions.keySet()) {
            if (input.matches(s)) {
                return accountOptions.get(s).getOption(new CustomMemberDataManager(new CustomDataConnection()), input);
            }
        }
        for (String s : commonOptions.keySet()) {
            if (input.matches(s)) {
                return commonOptions.get(s).getOption(new CustomBookDataManager(new CustomDataConnection()), input);
            }
        }
        for (String s : bookOptions.keySet()) {
            if (input.matches(s)) {
                return bookOptions.get(s).getOption(new CustomBookDataManager(new CustomDataConnection()), input);
            }
        }
        for (String s : boardGameOptions.keySet()) {
            if (input.matches(s)) {
                return boardGameOptions.get(s).getOption(new CustomGameDataManager(new CustomDataConnection()), input);
            }
        }
        return new InvalidOption();
    }

    public static OptionFactory getInstance() {
        return factory;
    }

    public Option addAccountOption(String name, AbstractAccountOption accountOption) {
        return accountOptions.containsKey(name) ? accountOptions.get(name) : accountOptions.put(name, accountOption);
    }

    public Option addCommonOption(String name, ItemAbstractOption commonOption) {
        return commonOptions.containsKey(name) ? commonOptions.get(name) : commonOptions.put(name, commonOption);
    }

    public Option addBookOption(String name, BookAbstractOption bookOption) {
        return bookOptions.containsKey(name) ? bookOptions.get(name) : bookOptions.put(name, bookOption);
    }

    public Option addBoardGameOption(String name, BoardGameAbstractOption boardGameOption) {
        return boardGameOptions.containsKey(name) ? boardGameOptions.get(name)
                : boardGameOptions.put(name, boardGameOption);
    }

}
