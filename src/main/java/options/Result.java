package options;

import users.User;

public class Result<T> {
    private T result;
    private boolean isSuccesfulOperation;
    private String message;
    private User user;
    private boolean isRoleModified;
    private Exception exceptionThrown;

    public Result(T result, boolean isSuccesfulOperation, String message, User user, boolean isRoleModified,
            Exception exceptionThrown) {
        super();
        this.result = result;
        this.isSuccesfulOperation = isSuccesfulOperation;
        this.message = message;
        this.user = user;
        this.isRoleModified = isRoleModified;
        this.exceptionThrown = exceptionThrown;
    }

    public T getResult() {
        return result;
    }

    public boolean isSuccesfulOperation() {
        return isSuccesfulOperation;
    }

    public User getUser() {
        return user;
    }

    public boolean isRoleModified() {
        return isRoleModified;
    }

    public Exception getExceptionThrown() {
        return exceptionThrown;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((exceptionThrown == null) ? 0 : exceptionThrown.hashCode());
        result = prime * result + (isRoleModified ? 1231 : 1237);
        result = prime * result + (isSuccesfulOperation ? 1231 : 1237);
        result = prime * result + ((message == null) ? 0 : message.hashCode());
        result = prime * result + ((this.result == null) ? 0 : this.result.hashCode());
        result = prime * result + ((user == null) ? 0 : user.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Result other = (Result) obj;
        if (exceptionThrown == null) {
            if (other.exceptionThrown != null)
                return false;
        } else if (!exceptionThrown.getClass().equals(other.exceptionThrown.getClass()))
            return false;
        if (isRoleModified != other.isRoleModified)
            return false;
        if (isSuccesfulOperation != other.isSuccesfulOperation)
            return false;
        if (message == null) {
            if (other.message != null)
                return false;
        } else if (!message.equals(other.message))
            return false;
        if (result == null) {
            if (other.result != null)
                return false;
        } else if (!result.equals(other.result))
            return false;
        else if (!(user instanceof User && other.user instanceof User) && user != other.user)
            return true;
        if (user == null) {
            if (other.user != null)
                return false;
        } else if (!(user instanceof User && other.user instanceof User) && user == other.user)
            return true;
        return true;

    }

    @Override
    public String toString() {
        if (!isSuccesfulOperation) {
            return "Unsuccessful Operation";
        }
        return message + "\n" + result.toString();

    }

}
