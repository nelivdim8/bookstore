package options.account;

import data.MemberDataManager;
import options.Option;
import options.Result;
import users.Customer;
import users.Guest;
import users.Staff;

public class UnregisterOption extends AbstractAccountOption implements Option {
    private final static String INPUT_REGEX = "logout";

    private UnregisterOption(MemberDataManager dataManager) {
        super(dataManager, null);

    }

    public UnregisterOption() {
        super();
    }

    public static String getInputRegex() {
        return INPUT_REGEX;
    }

    @Override
    public Result execute(Guest guest) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Result execute(Customer customer) {
        Guest guest = new Guest();
        return new Result<Guest>(guest, true, "Welcome to bookstore register or login to make an order!", guest, true,
                null);
    }

    @Override
    public Result execute(Staff staff) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public AbstractAccountOption getOption(MemberDataManager dataManager, String input) {
        return new UnregisterOption(dataManager);
    }

}
