package options.account;

import java.sql.SQLException;

import data.MemberDataManager;
import options.Option;
import options.Result;
import users.Customer;
import users.Guest;
import users.Role;
import users.Staff;

public class RegisterOption extends AbstractAccountOption implements Option {
    private final static String INPUT_REGEX = "register '[A-Z][a-z]*' '[A-Z][a-z]*' '[a-zA-Z1-9_.].*'"
            + " '[a-z1-9A-Z].*' '[a-z1-9A-Z].*@[a-z]{2,5}.[a-z]{2,4}' '[A-Z][a-z]*'";

    private RegisterOption(MemberDataManager dataManager, String input) {
        super(dataManager, input);

    }

    public RegisterOption() {
        super();
    }

    public static String getInputRegex() {
        return INPUT_REGEX;
    }

    @Override
    public Result execute(Guest guest) {
        String[] values = super.splitInput(super.getInput());
        boolean isSuccessful = false;
        try {
            isSuccessful = super.getDataManager().registerUser(values[0], values[1], values[2], values[3], values[4],
                    Role.valueOf(values[5]));
        } catch (SQLException e) {
            return new Result<Boolean>(isSuccessful, false, "Unsuccessful registration", guest, false, e);
        }
        return new Result<Boolean>(isSuccessful, true, "Successful registration", guest, false, null);
    }

    @Override
    public Result execute(Customer customer) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Result execute(Staff staff) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public AbstractAccountOption getOption(MemberDataManager dataManager, String input) {
        return new RegisterOption(dataManager, input);
    }

}
