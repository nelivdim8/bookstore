package options.account;

import data.MemberDataManager;
import options.AbstractOption;
import options.Option;

public abstract class AbstractAccountOption extends AbstractOption implements Option {
    private MemberDataManager dataManager;

    public AbstractAccountOption(MemberDataManager dataManager, String input) {
        super(input);
        this.dataManager = dataManager;
    }

    public AbstractAccountOption() {
        super();

    }

    public MemberDataManager getDataManager() {
        return dataManager;
    }

    public abstract AbstractAccountOption getOption(MemberDataManager dataManager, String input);

}
