package options.account;

import java.sql.SQLException;

import data.MemberDataManager;
import data.UnexistingUserException;
import options.Option;
import options.Result;
import users.Customer;
import users.Guest;
import users.Member;
import users.Staff;
import users.UnexistingRoleException;

public class LogInOption extends AbstractAccountOption implements Option {
    private final static String INPUT_REGEX = "login '[a-zA-Z1-9_.].*' '[a-z1-9A-Z].*'";

    private LogInOption(MemberDataManager dataManager, String input) {
        super(dataManager, input);

    }

    public LogInOption() {
        super();
    }

    public static String getInputRegex() {
        return INPUT_REGEX;
    }

    @Override
    public Result execute(Guest guest) {
        String[] values = super.splitInput(super.getInput());
        Member member = null;
        try {
            member = super.getDataManager().logIn(values[0], values[1]);
        } catch (SQLException | UnexistingUserException | UnexistingRoleException e) {
            e.printStackTrace();
            return new Result<Member>(member, false, "Unsuccessful login", member, true, e);
        }
        return new Result<Member>(member, true, "Welcome " + member.getUsername(), member, true, null);
    }

    @Override
    public Result execute(Customer customer) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Result execute(Staff staff) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public AbstractAccountOption getOption(MemberDataManager dataManager, String input) {
        return new LogInOption(dataManager, input);
    }

}
