package options.book;

import data.BookDataManager;
import options.AbstractOption;
import options.Option;

public abstract class BookAbstractOption extends AbstractOption implements Option {
    private BookDataManager bookData;

    public BookAbstractOption(BookDataManager bookData, String input) {
        super(input);
        this.bookData = bookData;
    }

    public BookAbstractOption() {
        super();

    }

    public BookDataManager getBookData() {
        return bookData;
    }

    public abstract BookAbstractOption getOption(BookDataManager bookData, String input);

}
