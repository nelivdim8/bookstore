package options.book;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import data.BookDataManager;
import items.Item;
import options.Option;
import options.Result;
import users.Customer;
import users.Guest;
import users.Staff;

public class SearchForBookByAuthorNameOption extends BookAbstractOption implements Option {
    private final static String INPUT_REGEX = "search book by author '[A-Z][a-z\\s]*\\s[A-Z][a-z\\s]*'";

    private SearchForBookByAuthorNameOption(BookDataManager bookData, String input) {
        super(bookData, input);

    }

    public SearchForBookByAuthorNameOption() {
        super();
    }

    public static String getInputRegex() {
        return INPUT_REGEX;
    }

    @Override
    public Result execute(Guest guest) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Result execute(Customer customer) {
        String[] values = super.splitInput(super.getInput());
        boolean isSuccessful = false;
        List<Item> books = new ArrayList<>();
        try {
            books = super.getBookData().getBookByAuthorName(values[0]);
        } catch (NumberFormatException | SQLException e) {
            return new Result<List<Item>>(books, false, "Unsuccessful search!", customer, false, e);
        }
        return new Result<List<Item>>(books, true, "Result from serach:", customer, false, null);
    }

    @Override
    public Result execute(Staff staff) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public BookAbstractOption getOption(BookDataManager bookData, String input) {
        return new SearchForBookByAuthorNameOption(bookData, input);
    }
}
