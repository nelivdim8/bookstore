package options.book;

import java.sql.SQLException;

import data.BookDataManager;
import options.Option;
import options.Result;
import users.Customer;
import users.Guest;
import users.Staff;

public class RequestNewBookOption extends BookAbstractOption implements Option {
    private final static String INPUT_REGEX = "request book '[A-Z][a-z\\s]*' '[A-Z][a-z\\s]*' '[1-9][0-9]*' '(?:yes|no)'";

    private RequestNewBookOption(BookDataManager bookData, String input) {
        super(bookData, input);

    }

    public RequestNewBookOption() {
        super();
    }

    public static String getInputRegex() {
        return INPUT_REGEX;
    }

    @Override
    public Result execute(Guest guest) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Result execute(Customer customer) {
        String[] values = super.splitInput(super.getInput());
        boolean isSuccessful = false;
        boolean isElectronic = (values[3].equals("yes")) ? true : false;
        try {
            isSuccessful = super.getBookData().requestForABook(customer, values[0], values[1],
                    Integer.parseInt(values[2]), isElectronic);
        } catch (NumberFormatException | SQLException e) {
            e.printStackTrace();
            return new Result<Boolean>(isSuccessful, false, "Unsuccessful request!", customer, false, e);
        }
        return new Result<Boolean>(isSuccessful, true, "Successful request!", customer, false, null);
    }

    @Override
    public Result execute(Staff staff) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public BookAbstractOption getOption(BookDataManager bookData, String input) {
        return new RequestNewBookOption(bookData, input);
    }

}
