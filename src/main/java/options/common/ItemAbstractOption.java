package options.common;

import java.sql.SQLException;

import data.ItemDataManager;
import options.AbstractOption;
import options.Option;

public abstract class ItemAbstractOption extends AbstractOption implements Option {
    private ItemDataManager dataManager;

    public ItemAbstractOption(ItemDataManager dataManager, String input) {
        super(input);
        this.dataManager = dataManager;

    }

    public ItemAbstractOption() {
        super();

    }

    public ItemDataManager getDataManager() {
        return dataManager;
    }

    public abstract ItemAbstractOption getOption(ItemDataManager dataManager, String input) throws SQLException;

}
