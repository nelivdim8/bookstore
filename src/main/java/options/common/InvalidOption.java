package options.common;

import options.Option;
import options.Result;
import users.Customer;
import users.Guest;
import users.Staff;

public class InvalidOption implements Option {

    @Override
    public Result execute(Customer customer) {
        return new Result<Boolean>(false, false, "Invalid input!", customer, false, null);
    }

    @Override
    public Result execute(Guest guest) {
        return new Result<Boolean>(false, false, "Invalid input!", guest, false, null);
    }

    @Override
    public Result execute(Staff staff) {
        return new Result<Boolean>(false, false, "Invalid input!", staff, false, null);
    }

}
