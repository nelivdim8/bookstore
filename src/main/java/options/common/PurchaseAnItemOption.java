package options.common;

import java.sql.SQLException;

import data.ItemDataManager;
import options.Option;
import options.Result;
import users.Customer;
import users.Guest;
import users.Staff;

public class PurchaseAnItemOption extends ItemAbstractOption implements Option {
    private final static String INPUT_REGEX = "purchase '[1-9][0-9]*' '[1-9][0-9]*'";

    private PurchaseAnItemOption(ItemDataManager dataManager, String input) {
        super(dataManager, input);

    }

    public PurchaseAnItemOption() {
        super();
    }

    public static String getInputRegex() {
        return INPUT_REGEX;
    }

    @Override
    public Result execute(Guest guest) {
        // TODO Auto-generated method stub;
        return null;
    }

    @Override
    public Result execute(Customer customer) {
        boolean isSuccessful;
        String[] values = super.splitInput(super.getInput());
        try {
            isSuccessful = super.getDataManager().purchaseItemById(Integer.parseInt(values[0]),
                    Integer.parseInt(values[1]), customer.getUsername());
        } catch (SQLException e) {
            return new Result<Boolean>(false, false, "Unsuccessful purchase!", customer, false, e);
        }
        return new Result<Boolean>(isSuccessful, isSuccessful, "Successful purchase!", customer, false, null);

    }

    @Override
    public Result execute(Staff staff) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ItemAbstractOption getOption(ItemDataManager dataManager, String input) {
        return new PurchaseAnItemOption(dataManager, input);
    }

}
