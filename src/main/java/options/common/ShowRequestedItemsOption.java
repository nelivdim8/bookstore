package options.common;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import data.CustomBookDataManager;
import data.CustomDataConnection;
import data.CustomGameDataManager;
import data.ItemDataManager;
import items.Order;
import options.Option;
import options.Result;
import users.Customer;
import users.Guest;
import users.Staff;

public class ShowRequestedItemsOption extends ItemAbstractOption implements Option {
    private ItemDataManager dataManager;

    private final static String INPUT_REGEX = "show requests '[a-zA-Z1-9_.].*'";

    private ShowRequestedItemsOption(ItemDataManager dataManager, String input) {
        super(dataManager, input);

    }

    public ShowRequestedItemsOption() {
        super();
    }

    public static String getInputRegex() {
        return INPUT_REGEX;
    }

    @Override
    public Result execute(Guest guest) {
        // TODO Auto-generated method stub;
        return null;
    }

    @Override
    public Result execute(Customer customer) {
        List<Order> requests = new ArrayList<>();
        String[] values = super.splitInput(super.getInput());
        try {
            requests.addAll(dataManager.getRequestedItems(values[0]));
            dataManager = new CustomGameDataManager(new CustomDataConnection());
            requests.addAll(dataManager.getRequestedItems(values[0]));
        } catch (SQLException e) {
            e.printStackTrace();
            return new Result<List<Order>>(requests, false, "Unsuccessful search!", null, false, e);
        }

        return new Result<List<Order>>(requests, true, "Items requested: ", null, false, null);
    }

    @Override
    public Result execute(Staff staff) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ItemAbstractOption getOption(ItemDataManager dataManager, String input) throws SQLException {
        return new ShowRequestedItemsOption(new CustomBookDataManager(new CustomDataConnection()), input);
    }

}
