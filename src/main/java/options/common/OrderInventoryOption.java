package options.common;

import java.sql.SQLException;

import data.ItemDataManager;
import options.Option;
import options.Result;
import users.Customer;
import users.Guest;
import users.Staff;

public class OrderInventoryOption extends ItemAbstractOption implements Option {
    private final static String INPUT_REGEX = "order inventory '[1-9][0-9]*' '[1-9][0-9]*'";

    private OrderInventoryOption(ItemDataManager dataManager, String input) {
        super(dataManager, input);

    }

    public OrderInventoryOption() {
        super();
    }

    public static String getInputRegex() {
        return INPUT_REGEX;
    }

    @Override
    public Result execute(Guest guest) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Result execute(Customer customer) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Result execute(Staff staff) {
        boolean isSuccessful = false;
        String[] values = super.splitInput(super.getInput());
        try {
            isSuccessful = super.getDataManager().orderAnItem(Integer.parseInt(values[0]), Integer.parseInt(values[1]));
        } catch (SQLException e) {
            return new Result<Boolean>(isSuccessful, false, "Order is not successful!", staff, false, e);
        }
        return new Result<Boolean>(isSuccessful, isSuccessful, "Successful order", staff, false, null);
    }

    @Override
    public ItemAbstractOption getOption(ItemDataManager dataManager, String input) {
        return new OrderInventoryOption(dataManager, input);
    }

}
