package items;

public class Book extends AbstractItem implements Item {
    private String title;
    private String author;
    private boolean isElectronic;
    private static ItemType type = ItemType.Book;

    public Book(int id, double price, String title, String author, boolean isElectronic) {
        super(price, id, type);
        this.title = title;
        this.author = author;
        this.isElectronic = isElectronic;
        // TODO Auto-generated constructor stub
    }

    public Book(int id, double price, String title, String author, int isElectronic) {
        super(price, id, type);
        this.title = title;
        this.author = author;
        this.isElectronic = isElectronic == 0 ? false : true;
        // TODO Auto-generated constructor stub
    }

    @Override
    public ItemType getType() {
        return ItemType.Book;
    }

    @Override
    public String toString() {
        return "Book [title=" + title + ", author=" + author + ", isElectronic=" + isElectronic + "]";
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public boolean isElectronic() {
        return isElectronic;
    }

}
