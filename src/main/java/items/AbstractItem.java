package items;

public abstract class AbstractItem implements Item {
    private double price;
    private static int counter = 0;
    private int id;
    private ItemType type;

    public AbstractItem(double price, ItemType type) {
        super();
        counter++;
        this.price = price;
        this.id = counter;
        this.type = type;
    }

    public AbstractItem(double price, int id, ItemType type) {
        super();
        this.price = price;
        this.id = id;
        this.type = type;
    }

    @Override
    public double getPrice() {
        return price;

    }

    @Override
    public int getId() {
        return id;
    }

}
