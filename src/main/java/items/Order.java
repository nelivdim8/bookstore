package items;

public class Order {
    private Item item;
    private int quantity;
    private String username;
    private Status status;

    public Order(Item item, int quantity, String username, Status status) {
        super();
        this.item = item;
        this.quantity = quantity;
        this.username = username;
        this.status = status;
    }

    public Item getItem() {
        return item;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getUsername() {
        return username;
    }

    public Status getStatus() {
        return status;
    }

}
