package items;

public interface Item {

    double getPrice();

    int getId();

    ItemType getType();

}
