package items;

public enum Status {
    Requested("Requested"), Accepted("Accepted"), Sent("Sent");
    private String name;

    private Status(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
