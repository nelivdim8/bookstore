package items;

public class BoardGame extends AbstractItem implements Item {
    private String name;
    private int minNumberOfPlayers;
    private int maxNumberOfPlayers;
    private static ItemType type = ItemType.Game;

    public BoardGame(double price, String name, int minNumberOfPlayers, int maxNumberOfPlayers) {
        super(price, type);
        this.name = name;
        this.minNumberOfPlayers = minNumberOfPlayers;
        this.maxNumberOfPlayers = maxNumberOfPlayers;
        // TODO Auto-generated constructor stub
    }

    public BoardGame(int id, double price, String name, int minNumberOfPlayers, int maxNumberOfPlayers) {
        super(price, id, type);
        this.name = name;
        this.minNumberOfPlayers = minNumberOfPlayers;
        this.maxNumberOfPlayers = maxNumberOfPlayers;
    }

    @Override
    public ItemType getType() {
        return ItemType.Game;
    }

    public String getName() {
        return name;
    }

    public int getMinNumberOfPlayers() {
        return minNumberOfPlayers;
    }

    public int getMaxNumberOfPlayers() {
        return maxNumberOfPlayers;
    }

    @Override
    public String toString() {
        return "BoardGame [name=" + name + ", minNumberOfPlayers=" + minNumberOfPlayers + ", maxNumberOfPlayers="
                + maxNumberOfPlayers + "]";
    }

}
