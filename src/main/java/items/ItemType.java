package items;

public enum ItemType {
    Book("Book"), Game("Game");
    private String name;

    private ItemType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
