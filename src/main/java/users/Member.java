package users;

public abstract class Member implements User {
    private String firstName;
    private String lastName;
    private String username;
    private String password;
    private String email;
    private Role role;

    public Member(String firstName, String lastName, String username, String password, String email, Role role) {
        super();
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.password = password;
        this.email = email;
        this.role = role;
    }

    public Member() {
        super();
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public String getUsername() {
        return username;
    }

    public abstract Member createMember(String firstName, String lastName, String username, String password,
            String email, Role role);

    @Override
    public String toString() {
        return "Member [firstName=" + firstName + ", lastName=" + lastName + ", username=" + username + ", password="
                + password + ", email=" + email + ", role=" + role + "]";
    }

}
