package users;

import options.Option;
import options.Result;

public class Staff extends Member implements User {
    public Staff(String firstName, String lastName, String username, String password, String email, Role role) {
        super(firstName, lastName, username, password, email, role);
        // TODO Auto-generated constructor stub
    }

    public Staff() {
        super();
    }

    @Override
    public Role getRole() {
        return Role.Staff;
    }

    @Override
    public Result executeOption(Option option) {
        return option.execute(this);
    }

    @Override
    public Member createMember(String firstName, String lastName, String username, String password, String email,
            Role role) {
        return new Staff(firstName, lastName, username, password, email, role);
    }
}
