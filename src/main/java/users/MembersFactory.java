package users;

import java.util.HashMap;
import java.util.Map;

public class MembersFactory {
    private Map<Role, Member> users = new HashMap<>();
    private static MembersFactory factory = new MembersFactory();

    private MembersFactory() {
        super();

    }

    public Member getUserByRole(Role role) throws UnexistingRoleException {
        if (users.containsKey(role)) {
            return users.get(role);

        }
        throw new UnexistingRoleException();
    }

    public Member addEntry(Role role, Member member) throws UnexistingRoleException {
        return users.put(role, member);

    }

    public static MembersFactory getInstance() {
        return factory;
    }

}
