package users;

import options.Option;
import options.Result;

public class Guest implements User {
    private Role role;

    @Override
    public Role getRole() {
        return Role.Guest;
    }

    @Override
    public Result executeOption(Option option) {
        return option.execute(this);
    }

    @Override
    public String toString() {
        return "Guest [role=" + role + "]";
    }

}
