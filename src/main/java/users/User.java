package users;

import options.Option;
import options.Result;

public interface User {
    Role getRole();

    Result executeOption(Option option);

}
