package users;

import options.Option;
import options.Result;

public class Customer extends Member implements User {
    public Customer(String firstName, String lastName, String username, String password, String email, Role role) {
        super(firstName, lastName, username, password, email, role);
        // TODO Auto-generated constructor stub
    }

    public Customer() {
        super();
    }

    @Override
    public Role getRole() {
        // TODO Auto-generated method stub
        return Role.Customer;
    }

    @Override
    public Result executeOption(Option option) {
        return option.execute(this);
    }

    @Override
    public Member createMember(String firstName, String lastName, String username, String password, String email,
            Role role) {
        return new Customer(firstName, lastName, username, password, email, role);
    }

}
