package users;

public enum Role {
    Guest("Guest"), Customer("Customer"), Staff("Staff");
    private String name;

    private Role(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
