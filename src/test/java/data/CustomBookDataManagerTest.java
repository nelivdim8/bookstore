package data;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import items.Book;
import items.Item;
import items.ItemType;
import junit.framework.TestCase;

@RunWith(MockitoJUnitRunner.class)
public class CustomBookDataManagerTest {
	@Mock
	private DataConnection dc;
	@Mock
	private Connection connection;
	@Mock
	private PreparedStatement statement;
	@Mock
	private ResultSet result;

	BookDataManager manager;

	@Test
	public void when_tryToGetBookByAuthorNameAndThereAreAvailable_expect_toReturnListOfThatItems() throws Exception {
	when(connection.prepareStatement(any(String.class))).thenReturn(statement);
		when(dc.getConnection()).thenReturn(connection);
		when(result.next()).thenReturn(true).thenReturn(false);
		when(result.getInt(1)).thenReturn(1);
		when(result.getString(2)).thenReturn("Book");
		when(result.getDouble(3)).thenReturn(10.00);
		when(result.getString(4)).thenReturn("Science");
		when(result.getString(5)).thenReturn("Ivan Ivanov");
		when(result.getInt(6)).thenReturn(0);
		when(statement.executeQuery()).thenReturn(result);
		Item item = new Book(1, 10.00, "Science", "Ivan Ivanov", 0);
		List<Item> items = new ArrayList<>();
		items.add(item);
		manager=new CustomBookDataManager(dc);
		assertEquals(items, manager.getBookByAuthorName("Ivan Ivanov"));

	}
	@Test
	public void when_tryToGetBookByTitleNameAndThereAreAvailable_expect_toReturnListOfThatItems() throws Exception {
	when(connection.prepareStatement(any(String.class))).thenReturn(statement);
		when(dc.getConnection()).thenReturn(connection);
		when(result.next()).thenReturn(true).thenReturn(false);
		when(result.getInt(1)).thenReturn(1);
		when(result.getString(2)).thenReturn("Book");
		when(result.getDouble(3)).thenReturn(10.00);
		when(result.getString(4)).thenReturn("Science");
		when(result.getString(5)).thenReturn("Ivan Ivanov");
		when(result.getInt(6)).thenReturn(0);
		when(statement.executeQuery()).thenReturn(result);
		Item item = new Book(1, 10.00, "Science", "Ivan Ivanov", 0);
		List<Item> items = new ArrayList<>();
		items.add(item);
		manager=new CustomBookDataManager(dc);
		assertEquals(items, manager.getBookByTitle("Science"));

	}
	
	@Test
	public void when_tryToInsetBookAndItIsNotAlreadyAvailable_expect_toReturnTrue() throws Exception {
	when(connection.prepareStatement(any(String.class))).thenReturn(statement);
		when(dc.getConnection()).thenReturn(connection);
		
		when(statement.executeUpdate()).thenReturn(1);
		
		manager=new CustomBookDataManager(dc);
		Book book = new Book(1, 10.00, "Science", "Ivan Ivanov", 0);
		assertTrue(manager.insertABook(book));

	}
	@Test
	public void when_tryToInsetBookAndItIsAlreadyAvailable_expect_toReturnFalse() throws Exception {
	when(connection.prepareStatement(any(String.class))).thenReturn(statement);
		when(dc.getConnection()).thenReturn(connection);
		
		when(statement.executeUpdate()).thenReturn(-1);
		
		manager=new CustomBookDataManager(dc);
		Book book = new Book(1, 10.00, "Science", "Ivan Ivanov", 0);
		assertFalse(manager.insertABook(book));

	}



}
